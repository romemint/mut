module.exports = function (sequelize, DataTypes) {
	var Project = sequelize.define('Project', {
		project_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
		name: DataTypes.STRING,
		address_no: DataTypes.STRING,
		district: DataTypes.STRING,
		county: DataTypes.STRING,
		province: DataTypes.STRING,
		zip_code: DataTypes.STRING,
		status: DataTypes.INTEGER
	}, {
		tableName: 'tbl_mut_project',
		updatedAt: 'last_update',
		createdAt: 'creation_date'
	});
	return Project;
};