module.exports = function(sequelize, DataTypes) {
  var AdminSystem = sequelize.define('AdminSystem', {
	  admin_system_id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
	  username: DataTypes.STRING,
	  password: DataTypes.STRING
  }, {
	  tableName: 'tbl_mut_admin_system',
	  updatedAt: 'last_update',
	  createdAt: 'creation_date'
  });

  return AdminSystem;
};
