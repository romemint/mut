module.exports = function (sequelize, DataTypes) {
	var Plate = sequelize.define('Plate', {
		plate_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
		project_id: DataTypes.INTEGER,
		name: DataTypes.STRING,
		width: DataTypes.STRING,
		height: DataTypes.STRING,
		thick: DataTypes.STRING,
		weight: DataTypes.STRING,
		strength: DataTypes.STRING,
		order_id: DataTypes.STRING,
		status: DataTypes.INTEGER,
		delivery_date: DataTypes.DATE,
		car_id: DataTypes.INTEGER
	}, {
		tableName: 'tbl_mut_plate',
		updatedAt: 'last_update',
		createdAt: 'creation_date'
	});

	Plate.associate = models => { 
		Plate.belongsTo(models.Car, { foreignKey: 'car_id' }); 
	}

	return Plate;
};