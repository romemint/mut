module.exports = function (sequelize, DataTypes) {
	var Car = sequelize.define('Car', {
		car_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
		car_type: DataTypes.STRING,
		car_no: DataTypes.STRING,
		support_weight: DataTypes.DECIMAL,
		car_width: DataTypes.DECIMAL,
		car_long: DataTypes.DECIMAL
	}, {
		tableName: 'tbl_mut_car',
		updatedAt: 'last_update',
		createdAt: 'creation_date'
	});

	Car.associate = models => { 
		Car.hasOne(models.Plate, { foreignKey: 'car_id' }); 
	}

	return Car;
};