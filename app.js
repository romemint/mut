var db = require('./models');
var express = require('express');
var compression = require('compression');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var errorHandler = require('errorhandler');
var app = express();
var port = process.env.PORT || 5678;
var aws = require('aws-sdk');
var pdf = require("html-pdf");
const uuidv4 = require('uuid/v4');
aws.config.loadFromPath('./AWSConfig.json');
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    next();
});
app.use(compression());
app.set('port', port);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json({
    extended: true
}));
app.use(methodOverride());
app.use(cookieParser());
app.use(session({
    secret: 'keyboard cat', cookie: { maxAge: 6000000 }, saveUninitialized: true, resave: true
}));
var env = process.env.NODE_ENV || 'development';
// development only
if ('development' == env) {
    console.log('dev mode!!')
}
var router = express.Router();

router.get('/', function (req, res) {
    res.redirect('/admin');
});
router.get("/admin/generatePdf/:id/:allPlateIds", async (req, res, next) => {
    const id = req.params.id;
    const allPlateIds = JSON.parse(req.params.allPlateIds);
    const plate = await db.Plate.findAll({
        where: { project_id: id, plate_id: allPlateIds },
        include: [{
            model: db.Car
        }]
    });
    const project = await db.Project.find({
        where: { project_id: id },
    });
    let cDate = new Date();
    let day = cDate.getDate();
    let month = cDate.getMonth() + 1;
    let year = cDate.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    let car = plate[0].Car;
    let formatDate = day + "/" + month + "/" + year;
    const stream = await new Promise((resolve, reject) => {
        var html = '<link href="https://fonts.googleapis.com/css?family=Kanit:300,200&subset=thai,latin" rel="stylesheet" type="text/css">';
        html += '<div style="margin-top: 100px;font-family: \'Kanit\', sans-serif;">';
        html += '	<h1 style="text-align: center;">บริษัท มหานคร พรีคาสต์ จำกัด</h1>';
        html += '	<h3 style="text-align: center;">Precast Delivery Form</h3>';
        html += '	<div style="display: flex;flex-wrap: wrap;display: -webkit-box;display: -webkit-flex;-webkit-flex-wrap: wrap;margin-left: 10%;margin-right: 10%;">';
        html += '		<div style="width: 33%;">';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">สำหรับ โครงการ: '+project.name+'</p>';
        html += '			<p style="font-size:11px;">Project ID: '+id+'</p>';
        html += '			<p style="font-size:11px;">บริษัท ขนส่ง: .......................................</p>';
        html += '			<p style="font-size:11px;">ต้นทาง: .......................................</p>';
        html += '			<p style="font-size:11px;">ทะเบียนรถ: '+car.car_no+'</p>';
        html += '			<p style="font-size:11px;">ชื่อคนขับรถ: .......................................</</p>';
        html += '		</div>';
        html += '		<div style="width: 33%;">';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">ชื่อเจ้าหน้าที่ประสานงาน: .......................................</p>';
        html += '			<p style="font-size:11px;">เบอร์ติดต่อ: .......................................</p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">ปลายทาง: .......................................</p>';
        html += '			<p style="font-size:11px;">ประเภท รถ: '+car.car_type+'</p>';
        html += '			<p style="font-size:11px;">ID คนขับรถ: .......................................</p>';
        html += '		</div>';
        html += '		<div style="width: 33%;">';
        html += '			<p style="font-size:11px;">เลขที่เอกสาร: .......................................</p>';
        html += '			<p style="font-size:11px;">วันที่ออกเอกสาร: '+formatDate+'</p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">ระยะทาง: .......................................</p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '		</div>';
        html += '	</div>';
        html += '	<div style="margin-left: 10%;margin-right: 10%;">';
        html += '		<table style="width: 100%;border-spacing: 0;" border="1">';
        html += '			<thead>';
        html += '				<th style="font-size:11px;">ลำดับ</th>';
        html += '				<th style="font-size:11px;">แผ่น ID</th>';
        html += '				<th style="font-size:11px;">ชือแผ่น</th>';
        html += '				<th style="font-size:11px;">กว้าง (m)</th>';
        html += '				<th style="font-size:11px;">หนา (m)</th>';
        html += '				<th style="font-size:11px;">สูง (m)</th>';
        html += '				<th style="font-size:11px;">น้ำหนัก(kg)</th>';
        html += '				<th style="font-size:11px;">Concrete Grade</th>';
        html += '			</thead>';
        html += '			<tbody id="plate-dom">';
        var index = 0;
        for(var p of plate){
            index++;
            html += '				<tr>';
            html += '					<td style="font-size:11px;">'+index+'</td>';
            html += '					<td style="font-size:11px;">'+p.plate_id+'</td>';
            html += '					<td style="font-size:11px;">'+p.name+'</td>';
            html += '					<td style="font-size:11px;">'+p.width+'</td>';
            html += '					<td style="font-size:11px;">'+p.thick+'</td>';
            html += '					<td style="font-size:11px;">'+p.height+'</td>';
            html += '					<td style="font-size:11px;">'+p.weight+'</td>';
            html += '					<td style="font-size:11px;">'+p.strength+'</td>';
            html += '				</tr>';
        }
        html += '			</tbody>';
        html += '		</table>';
        html += '	</div>';
        html += '	<div style="display: flex;flex-wrap: wrap;display: -webkit-box;display: -webkit-flex;-webkit-flex-wrap: wrap;margin-left: 10%;margin-right: 10%;">';
        html += '		<div style="width: 60%;">';
        html += '			<p style="font-size:11px;">รวม '+index+' แผ่น</p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">ผู้อนุมัติ: .......................................</p>';
        html += '			<p style="font-size:11px;">วันที่: .......................................</p>';
        html += '			<p style="font-size:11px;">Loaded Time: .......................................</p>';
        html += '			<p style="font-size:11px;">ETA: .......................................</p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">ได้รับผลิตภัณฑ์ครบถ้วนตามรายการข้างต้นแล้ว: .......................................</p>';
        html += '			<p style="font-size:11px;">ลายเซ็นผู้รับสินค้า: .......................................</p>';
        html += '		</div>';
        html += '		<div style="width: 40%;">';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">ผู้ออกเอกสาร: .......................................</p>';
        html += '			<p style="font-size:11px;">วันที่: .......................................</p>';
        html += '			<p style="font-size:11px;">คนขับรถ: .......................................</p>';
        html += '			<p style="font-size:11px;">วันที่: .......................................</p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">เวลารับสินค้า: .......................................</p>';
        html += '		</div>';
        html += '	</div>';
        html += '</div>';
        pdf.create(html).toStream((err, stream) => {
            if (err) {
                reject(reject);
                return;
            }
            resolve(stream);
        });
    });
    res.set({ 'Content-Type': 'application/pdf', 'Content-Length': stream.path })
    res.sendFile(stream.path);
});
router.get("/admin/generateReport/:id/:imageId/:allPlateIds", async (req, res, next) => {
    const id = req.params.id;
    const imageId = req.params.imageId;
    const allPlateIds = JSON.parse(req.params.allPlateIds);
    const plate = await db.Plate.findAll({
        where: { project_id: id, plate_id: allPlateIds },
        include: [{
            model: db.Car
        }]
    });
    const project = await db.Project.find({
        where: { project_id: id },
    });
    let cDate = new Date();
    let day = cDate.getDate();
    let month = cDate.getMonth() + 1;
    let year = cDate.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    let formatDate = day + "/" + month + "/" + year;
    const stream = await new Promise((resolve, reject) => {
        var html = '<link href="https://fonts.googleapis.com/css?family=Kanit:300,200&subset=thai,latin" rel="stylesheet" type="text/css">';
        html += '<div style="margin-top: 100px;font-family: \'Kanit\', sans-serif;">';
        html += '	<h1 style="text-align: center;">บริษัท มหานคร พรีคาสต์ จำกัด</h1>';
        html += '	<div style="display: flex;flex-wrap: wrap;display: -webkit-box;display: -webkit-flex;-webkit-flex-wrap: wrap;margin-left: 10%;margin-right: 10%;">';
        html += '		<div style="width: 50%;">';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">สำหรับ โครงการ: '+project.name+'</p>';
        html += '			<p style="font-size:11px;">ผู้ออกเอกสาร: Admin</p>';
        html += '		</div>';
        html += '		<div style="width: 50%;">';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">เลขที่เอกสาร: '+uuidv4()+'</p>';
        html += '			<p style="font-size:11px;">วันที่ออกเอกสำร: '+formatDate+'</p>';
        html += '		</div>';
        html += '	</div>';
        html += '	<div style="margin-left: 10%;margin-right: 10%;">';
        html += '		<table style="width: 100%;border-spacing: 0;" border="1">';
        html += '			<thead>';
        html += '				<th style="font-size:11px;">ลำดับ</th>';
        html += '				<th style="font-size:11px;">แผ่น ID</th>';
        html += '				<th style="font-size:11px;">ชือแผ่น</th>';
        html += '				<th style="font-size:11px;">กว้าง (m)</th>';
        html += '				<th style="font-size:11px;">หนา (m)</th>';
        html += '				<th style="font-size:11px;">สูง (m)</th>';
        html += '				<th style="font-size:11px;">น้ำหนัก(kg)</th>';
        html += '				<th style="font-size:11px;">Concrete Grade</th>';
        html += '			</thead>';
        html += '			<tbody id="plate-dom">';
        var index = 0;
        for(var p of plate){
            index++;
            html += '				<tr>';
            html += '					<td style="font-size:11px;">'+index+'</td>';
            html += '					<td style="font-size:11px;">'+p.plate_id+'</td>';
            html += '					<td style="font-size:11px;">'+p.name+'</td>';
            html += '					<td style="font-size:11px;">'+p.width+'</td>';
            html += '					<td style="font-size:11px;">'+p.thick+'</td>';
            html += '					<td style="font-size:11px;">'+p.height+'</td>';
            html += '					<td style="font-size:11px;">'+p.weight+'</td>';
            html += '					<td style="font-size:11px;">'+p.strength+'</td>';
            html += '				</tr>';
        }
        html += '			</tbody>';
        html += '		</table>';
        html += '	</div>';
        html += '	<div style="margin-top: 24px;margin-left: 10%;margin-right: 10%;width: 100%;">';
        html += '       <img src="https://teamsite-bucket.s3.amazonaws.com/static/common/imgscontent/meals/'+imageId+'" alt="Trulli" width="80%">';
        html += '	</div>';
        html += '	<div style="display: flex;flex-wrap: wrap;display: -webkit-box;display: -webkit-flex;-webkit-flex-wrap: wrap;margin-left: 10%;margin-right: 10%;">';
        html += '		<div style="width: 60%;">';
        html += '			<p style="font-size:11px;>ผู้ควบคุมการผลิต</p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">ลายเซ็นผู้ควบคุมการผลิต</p>';
        html += '			<p style="font-size:11px;">................................</p>';
        html += '			<p style="font-size:11px;">วันที่............................</p>';
        html += '			<p style="font-size:11px;">ผู้ตรวจสอบสินค้าเมื่อผลิตเสร็จ ........</p>';
        html += '			<p style="font-size:11px;">ลายเซ็นผู้ตรวจสอบสินค้า</p>';
        html += '			<p style="font-size:11px;">................................</p>';
        html += '			<p style="font-size:11px;">วันที่............................</p>';
        html += '		</div>';
        html += '		<div style="width: 40%;">';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;">ลายเซ็นผู้ออกเอกสาร</p>';
        html += '			<p style="font-size:11px;">................................</p>';
        html += '			<p style="font-size:11px;">วันที่............................</p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '			<p style="font-size:11px;"></p>';
        html += '		</div>';
        html += '	</div>';
        html += '</div>';
        pdf.create(html).toStream((err, stream) => {
            if (err) {
                reject(reject);
                return;
            }
            resolve(stream);
        });
    });
    console.log('stream',stream)
    res.set({ 'Content-Type': 'application/pdf', 'Content-Length': stream.path })
    res.sendFile(stream.path);
});
router.put('/api/auth', function (req, res) {
    let data = req.body;
    let resBody = {};
    db.AuthenticationLogin.update({
        token_facebook: data.token_facebook,
        token_google: data.token_google
    }, { where: { authentication_login_id: data.authentication_login_id } }).then((res) => {
        resBody = res;
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally get user');
        res.json(resBody);
    });
});
router.put('/api/config', function (req, res) {
    let data = req.body;
    let resBody = {};
    db.Config.update({
        last_login: data.last_login
    }, { where: { authentication_login_id: data.authentication_login_id } }).then((res) => {
        resBody = res;
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally get user');
        res.json(resBody);
    });
});

router.get('/api/plate', function (req, res) {
    let data = req.query;
    let resBody = {};
    db.Plate.findAll(data.project_id ? {
        where: { project_id: data.project_id, status: data.status },
        include: [{
            model: db.Car
        }]
    } : {}).then((res) => {
        resBody = res;
    }).catch((err) => {
        resBody = err;
        console.log(err)
    }).finally(() => {
        console.log('finally get plate');
        res.json(resBody);
    });
});
router.get('/api/project', function (req, res) {
    let data = req.query;
    let resBody = {};
    db.Project.findAll(data.project_id ? { where: { project_id: data.project_id } } : {}).then((res) => {
        resBody = res;
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally get project');
        res.json(resBody);
    });
});
router.get('/api/car', function (req, res) {
    let data = req.query;
    let resBody = {};
    db.Car.findAll(data.car_id ? { where: { car_id: data.car_id } } : {}).then((res) => {
        resBody = res;
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally get car');
        res.json(resBody);
    });
});
function createORUpdatePlates(plates, index, func) {
    let data = plates[index];

    if (!data) {
        func.call(this, { success: true });
        return;
    }

    if (data.plate_id) {
        db.Plate.update({
            plate_id: data.plate_id,
            project_id: data.project_id,
            name: data.name,
            width: data.width,
            height: data.height,
            thick: data.thick,
            weight: data.weight,
            strength: data.strength,
            delivery_date: data.delivery_date,
            status: 1
        }, {
            where: { plate_id: data.plate_id }
        }).then((res) => {
        }).catch((err) => {
            func.call(this, err);
        }).finally(() => {
            console.log('finally update plate');
            createORUpdatePlates(plates, ++index, func);
        });
    } else {
        db.Plate.create({
            project_id: data.project_id,
            name: data.name,
            width: data.width,
            height: data.height,
            thick: data.thick,
            weight: data.weight,
            strength: data.strength,
            delivery_date: data.delivery_date,
            status: 1
        }).then((res) => {
        }).catch((err) => {
            func.call(this, err);
        }).finally(() => {
            console.log('finally create plate');
            createORUpdatePlates(plates, ++index, func);
        });
    }
}
router.post('/api/plates', function (req, res) {
    let data = req.body;
    console.log('data', data)
    createORUpdatePlates(data.plates, 0, (resBody) => {
        res.json(resBody);
    });
});
router.post('/api/project', function (req, res) {
    let data = req.body;
    let resBody = {};
    console.log('data', data)
    if (data.project_id) {
        db.Project.update({
            project_id: data.project_id,
            name: data.name,
            address_no: data.address_no,
            district: data.district,
            county: data.county,
            province: data.province,
            zip_code: data.zip_code,
            status: data.status
        }, {
            where: { project_id: data.project_id }
        }).then((res) => {
            resBody = res;
        }).catch((err) => {
            resBody = err;
        }).finally(() => {
            console.log('finally post project', resBody);
            res.json(resBody);
        });
    } else {
        db.Project.create({
            name: data.name,
            address_no: data.address_no,
            district: data.district,
            county: data.county,
            province: data.province,
            zip_code: data.zip_code,
            status: data.status
        }).then((res) => {
            resBody = res;
        }).catch((err) => {
            resBody = err;
        }).finally(() => {
            console.log('finally post project');
            res.json(resBody);
        });
    }
});
router.post('/api/car', function (req, res) {
    let data = req.body;
    let resBody = {};
    console.log('data', data)
    if (data.car_id) {
        db.Car.update({
            car_id: data.car_id,
            car_type: data.car_type,
            car_no: data.car_no,
            support_weight: data.support_weight,
            car_width: data.car_width,
            car_long: data.car_long
        }, {
            where: { car_id: data.car_id }
        }).then((res) => {
            resBody = res;
        }).catch((err) => {
            resBody = err;
        }).finally(() => {
            console.log('finally post car', resBody);
            res.json(resBody);
        });
    } else {
        db.Car.create({
            car_type: data.car_type,
            car_no: data.car_no,
            support_weight: data.support_weight,
            car_width: data.car_width,
            car_long: data.car_long
        }).then((res) => {
            resBody = res;
        }).catch((err) => {
            resBody = err;
        }).finally(() => {
            console.log('finally post car ', resBody);
            res.json(resBody);
        });
    }
});
router.post('/api/confirm-deliver-plates', function (req, res) {
    let data = req.body;
    let resBody = {};
    db.Plate.update({
        status: 6
    }, { where: { plate_id: data.plateIds } }).then((res) => {
        resBody = {
            success: true,
            res: res
        };
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally put car');
        res.json(resBody);
    });
});
router.post('/api/deliver-plates', function (req, res) {
    let data = req.body;
    let resBody = {};
    db.Plate.update({
        status: 5
    }, { where: { plate_id: data.plateIds } }).then((res) => {
        resBody = {
            success: true,
            res: res
        };
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally put car');
        res.json(resBody);
    });
});
router.post('/api/generate-verify-plates', async function (req, res) {
    let data = req.body;
    let resBody = {};
    if(data.pIds && data.pIds.length > 0){
        await db.Plate.update({
            status: 25
        }, { where: { plate_id: data.pIds } }).then((res) => {
            resBody = {
                success: true,
                res: res
            };
        }).catch((err) => {
            resBody = err;
        });
    }
    
    if(data.plateIds && data.plateIds.length > 0){
        await db.Plate.update({
            status: 26
        }, { where: { plate_id: data.plateIds } }).then((res) => {
            resBody = {
                success: true,
                res: res
            };
        }).catch((err) => {
            resBody = err;
        });
    }
   
    if(data.orderIds && data.orderIds.length > 0){
        await db.Plate.update({
            status: 26
        }, { where: { order_id: data.orderIds }, status: 25 }).then((res) => {
            resBody = {
                success: true,
                res: res
            };
        }).catch((err) => {
            resBody = err;
        });
    }

    res.json(resBody);
});
router.post('/api/generate-plates-success', function (req, res) {
    let data = req.body;
    let resBody = {};
    db.Plate.update({
        status: 3
    }, { where: { plate_id: data.plateIds } }).then((res) => {
        resBody = {
            success: true,
            res: res
        };
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally put car');
        res.json(resBody);
    });
});
router.post('/api/generate-plates', function (req, res) {
    let data = req.body;
    let resBody = {};
    db.Plate.update({
        status: 4
    }, { where: { plate_id: data.plateIds } }).then((res) => {
        resBody = {
            success: true,
            res: res
        };
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally put car');
        res.json(resBody);
    });
});
router.post('/api/transport-plates', function (req, res) {
    let data = req.body;
    let resBody = {};
    db.Plate.update({
        status: 2,
        car_id: data.carId,
        order_id: uuidv4()
    }, { where: { plate_id: data.plateIds } }).then((res) => {
        resBody = {
            success: true,
            res: res
        };
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally put car');
        res.json(resBody);
    });
});

router.get('/api/booking', function (req, res) {
    let data = req.query;
    let resBody = {};
    let booking
    if (data.id && data.userType === "user") {
        booking = db.Booking.find({
            where: { booking_id: JSON.parse(data.id) },
            include: [{
                model: db.Car,
                include: [
                    {
                        model: db.User
                    }
                ]
            },
            {
                model: db.Project
            }
            ]
        });
    } else if (data.id && data.userType === "partner") {
        booking = db.Booking.find({
            where: { booking_id: JSON.parse(data.id) },
            include: [{
                model: db.Project,
                include: [
                    {
                        model: db.Partner
                    }
                ]
            },
            {
                model: db.Car
            }
            ]
        });
    } else {
        res = "error userType is user or partner.";
    }
    booking.then((res) => {
        resBody = res;
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally get booking');
        res.json(resBody);
    });
});
router.post('/api/booking', function (req, res) {
    let data = req.body;
    let resBody = {};
    db.Booking.create({
        project_id: data.project_id,
        car_id: data.car_id,
        type: data.type,
        amount: data.amount,
        price_per_amount: data.price_per_amount,
        from: data.from,
        to: data.to,
        status: data.status
    }).then((res) => {
        resBody = res;
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally post booking');
        res.json(resBody);
    });
});
router.put('/api/booking', function (req, res) {
    let data = req.body;
    let resBody = {};
    db.Booking.update({
        project_id: data.project_id,
        car_id: data.car_id,
        type: data.type,
        amount: data.amount,
        price_per_amount: data.price_per_amount,
        from: data.from,
        to: data.to,
        status: data.status
    }, { where: { booking_id: data.booking_id } }).then((res) => {
        resBody = res;
    }).catch((err) => {
        resBody = err;
    }).finally(() => {
        console.log('finally put booking');
        res.json(resBody);
    });
});

//admin//


app.get('/admin', function (req, res) {
    res.render("admin/login", { title: 'MUT TH ADMIN' });
});
app.get('/admin/logout', function (req, res) {
    req.session.destroy(function (err) {
        // cannot access session here
    })
    res.redirect("/admin");
});
app.get('/admin/generate-created', function (req, res) {
    if (req.session.user) {
        let project;
        db.Project.findAll({
            where: { status: [0, 1] },
            order: [['creation_date', 'ASC']]
        }).then((res) => {
            project = res;
        }).catch((err) => {
            console.log('err', err);
        }).finally(() => {
            console.log('finally get project');
            res.render("admin/generate-created", { session: req.session, project: project });
        });
    } else {
        res.redirect("/admin");
    }
});
app.get('/admin/generate-createing', function (req, res) {
    if (req.session.user) {
        let project;
        db.Project.findAll({
            where: { status: [0, 1] },
            order: [['creation_date', 'ASC']]
        }).then((res) => {
            project = res;
        }).catch((err) => {
            console.log('err', err);
        }).finally(() => {
            console.log('finally get project');
            res.render("admin/generate-createing", { session: req.session, project: project });
        });
    } else {
        res.redirect("/admin");
    }
});
app.get('/admin/generate', function (req, res) {
    if (req.session.user) {
        let project;
        db.Project.findAll({
            where: { status: [0, 1] },
            order: [['creation_date', 'ASC']]
        }).then((res) => {
            project = res;
        }).catch((err) => {
            console.log('err', err);
        }).finally(() => {
            console.log('finally get project');
            res.render("admin/generate", { session: req.session, project: project });
        });
    } else {
        res.redirect("/admin");
    }
});
app.get('/admin/report', function (req, res) {
    if (req.session.user) {
        let project;
        db.Project.findAll({
            where: { status: [0, 1] },
            order: [['creation_date', 'ASC']]
        }).then((res) => {
            project = res;
        }).catch((err) => {
            console.log('err', err);
        }).finally(() => {
            console.log('finally get plate');
            res.render("admin/report", { session: req.session, project: project });
        });
    } else {
        res.redirect("/admin");
    }
});
app.get('/admin/confirm-deliver', function (req, res) {
    if (req.session.user) {
        let project;
        db.Project.findAll({
            where: { status: [0, 1] },
            order: [['creation_date', 'ASC']]
        }).then((res) => {
            project = res;
        }).catch((err) => {
            console.log('err', err);
        }).finally(() => {
            console.log('finally get plate');
            res.render("admin/confirm-deliver", { session: req.session, project: project });
        });
    } else {
        res.redirect("/admin");
    }
});
app.get('/admin/deliver', function (req, res) {
    if (req.session.user) {
        let project;
        db.Project.findAll({
            where: { status: [0, 1] },
            order: [['creation_date', 'ASC']]
        }).then((res) => {
            project = res;
        }).catch((err) => {
            console.log('err', err);
        }).finally(() => {
            console.log('finally get plate');
            res.render("admin/deliver", { session: req.session, project: project });
        });
    } else {
        res.redirect("/admin");
    }
});
app.get('/admin/plate', function (req, res) {
    if (req.session.user) {
        let project;
        db.Project.findAll({
            where: { status: [0, 1] },
            order: [['creation_date', 'ASC']]
        }).then((res) => {
            project = res;
        }).catch((err) => {
            console.log('err', err);
        }).finally(() => {
            console.log('finally get plate');
            res.render("admin/plate", { session: req.session, project: project });
        });
    } else {
        res.redirect("/admin");
    }
});
app.get('/admin/project', function (req, res) {
    if (req.session.user) {
        let project;
        db.Project.findAll({
            where: { status: [0, 1] },
            order: [['creation_date', 'ASC']]
        }).then((res) => {
            project = res;
        }).catch((err) => {
            console.log('err', err);
        }).finally(() => {
            console.log('finally get project');
            res.render("admin/project", { session: req.session, project: project });
        });
    } else {
        res.redirect("/admin");
    }
});
app.get('/admin/booking', function (req, res) {
    if (req.session.user) {
        let booking;
        db.Booking.findAll({
            order: [['last_update', 'DESC']]
        }).then((res) => {
            booking = res;
        }).catch((err) => {
            console.log('err', err);
        }).finally(() => {
            console.log('finally get booking', booking);
            res.render("admin/booking", { session: req.session, booking: booking });
        });
    } else {
        res.redirect("/admin");
    }
});
app.get('/admin/partner', function (req, res) {
    if (req.session.user) {
        let partner;
        db.Partner.findAll({
            where: { status: [0, 1] },
            order: [['last_update', 'DESC']]
        }).then((res) => {
            partner = res;
        }).catch((err) => {
            console.log('err', err);
        }).finally(() => {
            console.log('finally get partner');
            res.render("admin/partner", { session: req.session, partner: partner });
        });
    } else {
        res.redirect("/admin");
    }
});
app.get('/admin/user', function (req, res) {
    if (req.session.user) {
        let user;
        db.User.findAll({
            where: { status: [0, 1] },
            order: [['last_update', 'DESC']]
        }).then((res) => {
            user = res;
        }).catch((err) => {
            console.log('err', err);
        }).finally(() => {
            console.log('finally get user');
            res.render("admin/user", { session: req.session, user: user });
        });
    } else {
        res.redirect("/admin");
    }
});
app.get('/admin/add-project', function (req, res) {
    if (req.session.user) {
        var id = req.query.id;
        if (id) {
            db.Project.find({
                where: { project๘รก: id }
            }).then(function (_info) {
                res.render("admin/add_project", { session: req.session, info: _info });
            }).catch(function (err) {
                logger.error(err);
            }).finally(function () {
            });
        } else {
            res.render("admin/add_project", { session: req.session, info: null });
        }
    }
    else {
        res.redirect("/admin");
    }
});
////////
app.post('/admin', function (req, res) {
    db.AdminSystem.find({ where: { username: req.body.username, password: req.body.password } }).then(function (user) {
        req.session.user = user;
        res.redirect("/admin/project");
    }).catch(function (err) {
        console.log(err);
        res.json({ success: false, error: err });
    });
});
app.get('/test', function (req, res) {
    db.Project.find({
    }).then(function (_info) {
        res.json(_info);
    }).catch(function (err) {
        logger.error(err);
    }).finally(function () {
    });
});
/*
* Respond to GET requests to /sign_s3.
* Upon request, return JSON containing the temporarily-signed S3 request and the
* anticipated URL of the image.
*/
app.get('/admin/sign_s3', function (req, res) {
    var S3_BUCKET = 'teamsite-bucket';
    var s3 = new aws.S3();
    var s3_params = {
        Bucket: S3_BUCKET,
        Key: req.query.s3_object_name,
        Expires: 3600,
        ContentType: req.query.s3_object_type,
        ACL: 'public-read'
    };
    s3.getSignedUrl('putObject', s3_params, function (err, data) {
        if (err) {
            //console.log(err);
            logger.error(err);
        }
        else {
            var return_data = {
                signed_request: data,
                url: 'https://' + S3_BUCKET + '.s3.amazonaws.com/' + req.query.s3_object_name
            };
            res.write(JSON.stringify(return_data));
            res.end();
        }
    });
});
/////////

app.use('/', router);
app.use(errorHandler());
app.listen(port);
console.log('Server running at http://127.0.0.1:' + port + '/');

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}