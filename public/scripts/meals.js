$(document).ready(function() {
	var id = getParameterByName("id");
	var city = getParameterByName("city");
	var cabin = getParameterByName("cabin");
	var from = getParameterByName("from");
	var to = getParameterByName("to");


	$(document).on("click", "#btn_meals", function(){

		var pnr = $("#pnr").val();
		var lastname = $("#lastname").val();

		if(pnr.length == 6 && lastname.length > 1){
			if(location.href.indexOf("login") == -1){
				$('#loginModal').modal('hide');	
			}

			$.blockUI({
				message: $('#displayBox'),
				css: {
						top:  ($(window).height() - 120) /2 + 'px',
						left: ($(window).width() - 120) /2 + 'px',
						width: '120px'
				}
			});

			//check if PNR is valid
			$.ajax({
				method: 'GET',
				url: '/get_pnr',
				data: {pnr_no: pnr},
				success: function(data){
					if(data.success){
						data.pnr = pnr;

						var jsonStr = JSON.stringify( data );
						sessionStorage.setItem( "pnr", jsonStr );
						location.href = "/step1.html";
					}
					else{
						if(data.data){
							alert(data.data['ns2:ErrDesc']);
						}
						else{
							alert(data.error);
						}
					}
				},
				complete: function(){
					$.unblockUI();
				}
			});
		}
		else{
			alert("Please enter a valid PNR and last name");
		}

		// location.href = "/step1.html";

	});



	if(id){
		$.ajax({
			type: 'GET',
			url: '/api/get_meal',
			data: {
				id: id
			},
			success: function(data){
				if(data.menu){
					var image = "https://teamsite-bucket.s3.amazonaws.com/static/common/imgscontent/meals/menu-no-image.jpg";
					if(data.menu.image){
						image = data.menu.image;
					}

					if(data.menu.cabin_class == "F"){
						$("#cabin_class_label").html("FIRST CLASS MEAL");
					}
					else{
						$("#cabin_class_label").html("BUSINESS CLASS MEAL");
					}

					$("#product_name").html(data.menu.product_name);
					$("#image").css("background", "url(" + image +") center no-repeat");
					$("#image").css("background-size", "cover");
					$("#description").html(data.menu.description);
				}
				else if(data.error){
					alert(data.error);
				}
				else{
					alert("No meals found");
				}
			},
			complete: function(){
				$.unblockUI();
			}
		});
	}


	if(city){
		$.blockUI({
				message: $('#displayBox'),
				css: {
						top:  ($(window).height() - 120) /2 + 'px',
						left: ($(window).width() - 120) /2 + 'px',
						width: '120px'
				}
		});

		$("#origin_city").val(city);
		$("#cabin_class").val(cabin);

		$.ajax({
			type: 'GET',
			url: '/api/get_meals',
			data: {
				origin_city: city, cabin_class: cabin
			},
			success: function(data){
				if(data.menu.length > 0){
					$(".mealthumb").empty();

					var html = '<div class="row-fluid">';

						$.each(data.menu, function(){
							var image = "https://teamsite-bucket.s3.amazonaws.com/static/common/imgscontent/meals/menu-no-image.jpg";
							if(this.image){
								image = this.image;
							}

							html += '<div class="span3 text-center">' +
											'<a href="/mealdetails.html?id=' + this.pk_id + '"><div style="background: url(' + image + ') center no-repeat; background-size: cover; height: 160px; width: 160px; margin: 0 auto;"></div>' +
											'<h3>' + this.product_name + '</h3></a>' +
											'<div class="span11"><a href="/mealdetails.html?id=' + this.pk_id + '" class="THAI-btn-purple-bg-white-text pull-right">Meal details</a></div>' +
										'</div>';
						});
						html += '</div>';

						$(".mealthumb").append(html);
				}
				else if(data.error){
					alert(data.error);
				}
				else{
					alert("No meals found");
				}
			},
			complete: function(){
				$.unblockUI();
			}
		});
	}


	$(document).on("click", "#search_meals", function(){

		var city = $("#origin_city").val();
		var cabin = $("#cabin_class").val();
		var from = $("#datefrom").val();
		var to = $("#dateto").val();

		location.href = "?city=" + city + "&cabin=" + cabin;

		// $.blockUI({
		// 		message: $('#displayBox'),
		// 		css: {
		// 				top:  ($(window).height() - 120) /2 + 'px',
		// 				left: ($(window).width() - 120) /2 + 'px',
		// 				width: '120px'
		// 		}
		// });

		// $.ajax({
		// 	type: 'GET',
		// 	url: '/api/get_meals',
		// 	data: {
		// 		origin_city: $("#origin_city").val(), cabin_class: $("#cabin_class").val()
		// 	},
		// 	success: function(data){
		// 		if(data.menu.length > 0){
		// 			$(".mealthumb").empty();

		// 			var html = '<div class="row-fluid">';

		// 				$.each(data.menu, function(){
		// 					html += '<div class="span3 text-center">' +
		// 									'<a href="/mealdetails.html?id=' + this.pk_id + '"><div style="background: url(' + this.image + ') center no-repeat; background-size: cover; height: 160px; width: 160px; margin: 0 auto;"></div>' +
		// 									'<h3>' + this.product_name + '</h3></a>' +
		// 									'<div class="span11"><a href="/mealdetails.html?id=' + this.pk_id + '" class="THAI-btn-purple-bg-white-text pull-right">Meal details</a></div>' +
		// 								'</div>';
		// 				});
		// 				html += '</div>';

		// 				$(".mealthumb").append(html);
		// 		}
		// 		else if(data.error){
		// 			alert(data.error);
		// 		}
		// 		else{
		// 			alert("No meals found");
		// 		}
		// 	},
		// 	complete: function(){
		// 		$.unblockUI();
		// 	}
		// });

	});

	//show select meals page
	var type = getParameterByName("type");
	var origin = getParameterByName("origin");

	if(type){
		$.blockUI({
				message: $('#displayBox'),
				css: {
						top:  ($(window).height() - 120) /2 + 'px',
						left: ($(window).width() - 120) /2 + 'px',
						width: '120px'
				}
		});

		//update heading
		if(type == 'E' || type == 'B'){
			$("#menu_type").html("Business Class Menu");
		}
		else{
			$("#menu_type").html("First Class Menu");
		}

		$.ajax({
			type: 'GET',
			url: '/api/get_meals',
			data: {
				origin_city: origin, cabin_class: type == 'E' ? 'B' : type
			},
			success: function(data){
				if(data.menu.length > 0){
					$(".mealthumb").empty();

					var html = '<div class="row-fluid">';

						$.each(data.menu, function(){
							var image = "https://teamsite-bucket.s3.amazonaws.com/static/common/imgscontent/meals/menu-no-image.jpg";
							if(this.image){
								image = this.image;
							}

							html += '<div class="span3 text-center">' +
											'<a href="/mealdetails.html?id=' + this.pk_id + '"><div style="background: url(' + this.image + ') center no-repeat; background-size: cover; height: 160px; width: 160px; margin: 0 auto;"></div>' +
											'<input type="hidden" class="menu-spml" value="' + this.iatacode + '" />' +
											'<input type="hidden" class="menu-image" value="' + image + '" />' +
											'<input type="hidden" class="menu-description" value="' + this.description + '" />' +
											'<h3 class="meal-title">' + this.product_name + '</h3></a>';

							if(type == 'E'){
								html += '<p>Surcharge: <span class="meal-price">' + this.price_per_unit + ' THB </span></p>';
							}

							html +=	'<div class="span11"><a href="javascript:void(0)" class="THAI-btn-purple-bg-white-text pull-right order-now">Order Now</a></div>' +
										'</div>';
						});
						html += '</div>';

						$(".mealthumb").append(html);
				}
				else if(data.error){
					alert(data.error);
				}
				else{
					alert("No meals found");
				}
			},
			complete: function(){
				$.unblockUI();
			}
		});
	}


	// var cart = [
	// 	{name: "Standard Meal", price: '', passenger: 1},
	// 	{name: "", price: '', passenger: 2},
	// 	{name: "", price: '', passenger: 3},
	// 	{name: "Standard Meal", price: '', passenger: 4}
	// ];
	if(sessionStorage.getItem( "pnr" )){
		var pnrValue = sessionStorage.getItem( "pnr" );
		var pnr = JSON.parse( pnrValue );

		$("#pnr_code").html(pnr.pnr);

		var itin_data = pnr.itin_info;
		var html = "";

		$.each(itin_data, function(){
			var travel_data = this.data;
			var i = 1;
			var itin = this;

			var cabin_class = "";
			var class_code = itin.cabin_class;
			var meal_text = "";
			var button_text = "";

			if(class_code == 'M' || class_code == 'Q' || class_code == 'B' || class_code == 'V' || class_code == 'W' || class_code == 'E' || class_code == 'S'
				|| class_code == 'T' || class_code == 'L'){

				cabin_class = "Economy";
				class_code = "E";
				meal_text = "No special meal request";
				button_text = "Upgrade Meal";
			}
			else if(class_code == 'C' || class_code == 'D' || class_code == 'J'){

				cabin_class = "Business";
				class_code = "B";
				button_text = "Select Meal";
			}
			else if(class_code == 'P' || class_code == 'F'){

				cabin_class = "First";
				class_code = "F";
				button_text = "Select Meal";
			}
			else if(class_code == 'Y'){

				cabin_class = "Economy";
				class_code = "E";
				meal_text = "No special meal request";
				button_text = "Upgrade Meal";
			}
			else if(class_code == 'U'){

				cabin_class = "Economy";
				class_code = "E";
				meal_text = "No special meal request";
				button_text = "Upgrade Meal";
			}

			if(window.location.href.indexOf("step1") > -1){
				$.each(travel_data, function(){

					html += '<tr class="td-border-bottom">';
					if(i == 1){
						html += 	'<td class="bg-even-border" ' + (travel_data.length > 1 ? 'rowspan="2"' : "") + '>';
						html += 		itin.airline_code + ' ' + itin.flight_number + ':' + itin.departure_date + '<br>';
						html += 		itin.board_point + ' - ' + itin.off_point;
						html += 	'</td>';
					}
					html += 	'<td class="bg-even-border">' + this.title + ' ' + this.first_name + ' ' + this.last_name + ' (' + cabin_class + ')</td>';
					html += 	'<td class="bg-even-border" id="mealname_' + itin.ST + '.' + this.PT + '">&nbsp; ' + (this.meal_details ? this.meal_details : meal_text) + '</td>';
					html += 	'<td class="bg-even td-border-right"><a href="selectmeal.html?type=' + class_code + '&origin=' + itin.board_point + '&st=' + itin.ST + '&pt=' + this.PT + '" class="THAI-btn-purple-bg-white-text-small">' + button_text + '</a></td>';
					html += '</tr>';
					i++;
				});
				//$("#tablemeal").append(html);
			}
			if(window.location.href.indexOf("step3") > -1){
				$.each(travel_data, function(){

					html += '<tr class="td-border-bottom">';
					if(i == 1){
						html += 	'<td class="bg-even-border" ' + (travel_data.length > 1 ? 'rowspan="2"' : "") + '>';
						html += 		itin.airline_code + ' ' + itin.flight_number + ':' + itin.departure_date + '<br>';
						html += 		itin.board_point + ' - ' + itin.off_point;
						html += 	'</td>';
					}
					html += 	'<td class="bg-even-border">' + this.title + ' ' + this.first_name + ' ' + this.last_name + ' (' + cabin_class + ')</td>';
					html += 	'<td class="bg-even-border td-border-right" id="mealname_' + itin.ST + '.' + this.PT + '">&nbsp; ' + (this.meal_details ? this.meal_details : meal_text) + '</td>';
					//html += 	'<td class="bg-even td-border-right">' + (this.price ? this.price : "-") + '</td>';
					html += '</tr>';
					i++;
				});
				//$("#tableprice_per_unit").append(html);
			}
		});

		$("#tablemeal").append(html);
		$("#tableprice_per_unit").append(html);


		// $.each(cart, function(){
		// 	$("#mealname_" + this.passenger).html(this.name + "<br>" + this.price);
		// });

		// var jsonStr = JSON.stringify( pnr );
		// sessionStorage.setItem( "cart", jsonStr );
	}



	//on click event for Order Now button
	$(document).on("click", ".order-now", function(){
		if(sessionStorage.getItem("pnr")){
			var meal_item = $(this).parent().parent();
			var price = meal_item.find(".meal-price").text();
			var title = meal_item.find(".meal-title").text();
			var menu_spml = meal_item.find(".menu-spml").val();
			var menu_image = meal_item.find(".menu-image").val();
			var menu_description = meal_item.find(".menu-description").val();
			var st = getParameterByName("st");
			var pt = getParameterByName("pt");


			if(sessionStorage.getItem( "pnr" )){
				var pnrValue = sessionStorage.getItem( "pnr" );
				var pnr = JSON.parse( pnrValue );

				var itin_data = pnr.itin_info;

				$.each(itin_data, function(){
					var travel_data = this.data;
					var itin = this;


					$.each(travel_data, function(){

						if(itin.ST == st && this.PT == pt){
							this.meal_details = title;
							this.menu_spml = menu_spml;
							this.menu_image = menu_image;
							this.menu_description = menu_description;
							this.price = price;
						}

					});
				});

				var jsonStr = JSON.stringify( pnr );
				sessionStorage.setItem( "pnr", jsonStr );
			}


			location.href = "step1.html";
		}
		else{
			$('#loginModal').modal('show');	
		}
		
	});


	$(document).on('click', '#popup_terms', function(){
		$('#termsModal').modal('show');	
	});


	$("#btn_confirm").click(function(){
		$.blockUI({
				message: $('#displayBox'),
				css: {
						top:  ($(window).height() - 120) /2 + 'px',
						left: ($(window).width() - 120) /2 + 'px',
						width: '120px'
				}
		});
		
		var pnrValue = sessionStorage.getItem( "pnr" );
		
		var data = {pnr: pnrValue};
		
		$.ajax({
			type: 'POST',
			url: '/add_pnr',
			data: data,
			success: function(data){
				return_data = data;
				if(data.success){
					sessionStorage.removeItem( "pnr" );
					sessionStorage.setItem( "pnr_details", pnrValue );

					location.href = "step4.html";
				}
				else if(data.error){
					alert(data.error);
				}
				else{
					alert("Error while processing your request. Please try again later.");
				}
			},
			complete: function(){
				$.unblockUI();
			}
		});

		

	});

});

function getParameterByName(name) {
		var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
		return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
